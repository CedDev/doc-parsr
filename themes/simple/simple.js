const fs = require( 'fs' );
const path = require( 'path' );
const extra = require( 'fs-extra' );
const marked = require( 'marked' );

var Doc;

module.exports = function( doc, docPath ) {
    Doc = doc;

    //console.log( JSON.stringify( doc.api, null, 2 ) );

    var root = path.resolve( docPath );

    // Load templates used for each type of page of the documentation

        var templates = {
            class: fs.readFileSync( path.join( __dirname, "templates", "class.html" ), "utf8" ),
            module: fs.readFileSync( path.join( __dirname, "templates", "module.html" ), "utf8" ),
            manual: fs.readFileSync( path.join( __dirname, "templates", "manual.html" ), "utf8" )
        };

    // Build the side-main-menu

        var mainMenuManuals = (
            doc.manual.filter( ({ name }) => name.toLowerCase() != "home" ).length > 0 ?
                doc.manual.filter( ({ name }) => name.toLowerCase() != "home" ).map(
                    manual => ({ label: manual.name, url: `manual_${manual.name.replace( / /g, "_" )}.html` })
                )
            :   null
        );

        var mainMenuClasses = (
            doc.api.classes.length > 0 ?
                doc.api.classes.map(
                    cls => ({ label: cls.name, url: `class_${cls.name}.html` })
                )
            :   null
        );

        var mainMenuModules = (
            doc.api.modules.length > 0 ?
                doc.api.modules.map( mod => ({ label: mod.name, url: `module_${mod.name}.html` }) )
            :   null
        );

        var mainMenuSearch = `<li><input id="search" type="text" placeholder="Search..."></li>`;
        var mainMenuHome = "<li><a href='index.html'>Home</a></li>";
        var mainMenuGlobal = "<li><a href='global.html'>Global</a></li>";
        var mainNavigation = "";

        mainNavigation += mainMenuSearch;
        mainNavigation += mainMenuHome;
        mainNavigation += mainMenuGlobal;
        if( mainMenuManuals !== null ) mainNavigation += buildMainNavMenuGroup( "Manual", mainMenuManuals );
        if( mainMenuClasses !== null ) mainNavigation += buildMainNavMenuGroup( "Classes", mainMenuClasses );
        if( mainMenuModules !== null ) mainNavigation += buildMainNavMenuGroup( "Modules", mainMenuModules );

    // Make the global page ( a module called global )

        createModulePage( root, templates.module, mainNavigation, doc.api, "Global", path.join( root, "global.html" ) );

    // Loop through all the data and build all the pages

        let homep = doc.manual.filter( ({ name }) => name.toLowerCase() == "home" );
        if( homep.length == 0 ) doc.manual.push(
            {
                name: "Home",
                html: "<h1>Home</h1><p>Please add a markdown file with the first line reading <code>[//]: # ( Home )</code> to set your homepage."
            }
        );

        var homePage = false;
        doc.manual.forEach( manual => {
            if( manual.name.toLowerCase() == "home" ) {
                if( !homePage ) {
                    createHomePage( root, templates.manual, mainNavigation, manual );
                    homePage = true;
                    return;
                }
            }
            createManualPage( root, templates.manual, mainNavigation, manual );
        });

        if( !homePage ) console.log( "[ WARNING ]:".yellow, "Could not create index.html!" );

        doc.api.classes.forEach( cls => {
            createClassPage( root, templates.class, mainNavigation, cls );
        });

        doc.api.modules.forEach( mod => {
            createModulePage( root, templates.module, mainNavigation, mod );
        });

    // Copy the static content

        extra.copySync( path.join( __dirname, "css" ), path.join( root, "css" ) );
        extra.copySync( path.join( __dirname, "js" ), path.join( root, "js" ) );

    // Return 0 since no errors occured.

        return 0;
};

function buildMainNavMenuGroup( title, items ) {
    var html = `
        <li class="menu-group">
            <span>${ title }</span>
            <ul class="no-bullet">`;
                items.map( item => {
                    html += `<li data-term="${ item.label.toLowerCase() }"><a href="${ item.url }">${ item.label }</a></li>`
                });
                html += `
            </ul>
        </li>
    `;
    return html;
}

function createHomePage( root, tpl, mainNav, manual ) {
    var html = tpl
        .replace( "[[ MANUAL TITLE ]]", manual.name )
        .replace( "[[ MAIN NAVIGATION ]]", mainNav )
        .replace( "[[ MANUAL CONTENT ]]", m => generateLinks( manual.html/*.replace( /\$/g, "$$$$" )*/ ) )
    ;

    fs.writeFileSync( path.join( root, `index.html` ), html );
}

function createManualPage( root, tpl, mainNav, manual ) {
    var html = tpl
        .replace( "[[ MANUAL TITLE ]]", manual.name )
        .replace( "[[ MAIN NAVIGATION ]]", mainNav )
        .replace( "[[ MANUAL CONTENT ]]", m => generateLinks( manual.html/*.replace( /\$/g, "$$" )*/ ) )
    ;

    fs.writeFileSync( path.join( root, `manual_${manual.name.replace(/ /g,"_")}.html` ), html );
}

function generateLinks( text ) {
    var regex = /\[\[[^\[\]]*\]\]/g;
    var newText = text;

    while(( m = regex.exec( text ) ) !== null ) {
        if( m.index === regex.lastIndex ) regex.lastIndex++;
        m.forEach( match => {
            name = match.substring( 2, match.length-2 ).trim();
            var matches = name.split(".");

            if( matches.length == 1 ) {
                if( typeIsModule( name ) !== -1 ) newText = newText.replace(
                    match,
                    `<a href="module_${name.replace(/ /g,"_")}.html">${name}</a>`
                );
                else if( typeIsClass( name ) !== -1 ) newText = newText.replace(
                    match,
                    `<a href="class_${name.replace(/ /g,"_")}.html">${name}</a>`
                );
                else if( typeIsManual( name ) !== -1 ) newText = newText.replace(
                    match,
                    `<a href="manual_${name.replace(/ /g,"_")}.html">${name}</a>`
                );
            }
            else {
                var target = typeIsClass( matches[0] );
                if( target != -1 ) {
                    if( typeIsProperty( matches[1], target ) != -1 ) {
                        newText = newText.replace( match, `<a href="class_${matches[0]}.html#prop-${matches[1]}">${name}</a>` );
                    }
                    else if( typeIsMethod( matches[1], target ) != -1 ) {
                        newText = newText.replace( match, `<a href="class_${matches[0]}.html#method-${matches[1]}">${name}</a>` );
                    }
                }
                else {
                    target = typeIsModule( matches[0] );
                    if( target != -1 ) {
                        if( typeIsConstant( matches[1], target ) != -1 ) {
                            newText = newText.replace( match, `<a href="module_${matches[0]}.html#constant-${matches[1]}">${name}</a>`);
                        }
                        else if( typeIsVariable( matches[1], target ) != -1 ) {
                            newText = newText.replace( match, `<a href="module_${matches[0]}.html#variable-${matches[1]}">${name}</a>`);
                        }
                        else if( typeIsFunction( matches[1], target ) != -1 ) {
                            newText = newText.replace( match, `<a href="module_${matches[0]}.html#function-${matches[1]}">${name}</a>`);
                        }
                        else {
                            target = typeIsClass( matches[1], target );
                            if( target != -1 ) {
                                if( matches.length > 2 ) {
                                    if( typeIsProperty( matches[2], target ) != -1 ) {
                                        newText = newText.replace( match, `<a href="class_${matches[1]}.html#prop-${matches[2]}">${name}</a>` );
                                    }
                                    else if( typeIsMethod( matches[2], target ) != -1 ) {
                                        newText = newText.replace( match, `<a href="class_${matches[1]}.html#method-${matches[2]}">${name}</a>` );
                                    }
                                }
                                else {
                                    newText = newText.replace( match, `<a href="class_${matches[1]}.html">${name}</a>` );
                                }
                            }
                        }
                    }
                }
            }
        });
    }

    return newText;
}

function typeIsModule( name ) {
    var res = -1;
    Doc.api.modules.forEach( m => {
        if( m.name == name ) res = m;
    });
    return res;
}

function typeIsClass( name, module ) {
    if( typeof module === "undefined" ) module = Doc.api;
    var res = -1;
    if( !module.classes ) return res;
    module.classes.forEach( c => {
        if( c.name == name ) res = c;
    });
    return res;
}

function typeIsManual( name ) {
    var res = -1;
    Doc.manual.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function typeIsConstant( name, target ) {
    if( typeof target === "undefined" ) target = Doc.api;
    var res = -1;
    if( !target.contants ) return res;
    target.constants.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function typeIsVariable( name, target ) {
    if( typeof target === "undefined" ) target = Doc.api;
    var res = -1;
    if( !target.variables ) return res;
    target.variables.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function typeIsFunction( name, target ) {
    if( typeof target === "undefined" ) target = Doc.api;
    var res = -1;
    if( !target.fonctions ) return res;
    target.fonctions.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function typeIsProperty( name, target ) {
    if( typeof target === "undefined" ) target = Doc.api;
    var res = -1;
    if( !target.props ) return res;
    target.props.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function typeIsMethod( name, target ) {
    if( typeof target === "undefined" ) target = Doc.api;
    var res = -1;
    if( !( "methods" in target ) ) return res;
    target.methods.forEach( val => { if( val.name == name ) res = val; });
    return res;
}

function createClassPage( root, tpl, mainNav, cls ) {

    // Constructor
    let { syntax, parameters } = processFunction( cls.new, cls.name );
    let { propList, propData } = processProperties( cls.props );
    let { methodList, methodData } = processMethods( cls.methods );

    let className = cls.name;
    if( cls.extends ) {
        className += "<small> extends "+generateLinks( "[["+cls.extends.name+"]]" ).replace(/\[/g,"").replace(/\]/g,"")+"</small>";
    };

    var html = tpl
        .replace( "[[ MAIN NAVIGATION ]]", mainNav )
        .replace( "[[ CLASS NAME ]]", className )
        .replace( /\[\[ CLASS TITLE \]\]/g, cls.name )
        .replace( "[[ CLASS DESCRIPTION ]]", generateLinks( cls.description ) )
        .replace( "[[ CLASS CONSTRUCTOR SYNTAX ]]", `new ${syntax};` )
        .replace( "[[ CLASS CONSTRUCTOR DESCRIPTION ]]", cls.new ? cls.new.description || "" : "" )
        .replace( "[[ CLASS CONSTRUCTOR PARAMETERS ]]", parameters === "" ? "<tr><td><span class='none'>None</span></td></tr>" : parameters  )
        .replace( "[[ CLASS PROPERTIES ]]", propList )
        .replace( "[[ CLASS METHODS ]]", methodList )
        .replace( "[[ CLASS PROPERTIES INFO ]]", propData )
        .replace( "[[ CLASS METHODS INFO ]]", methodData )
    ;

    fs.writeFileSync( path.join( root, `class_${cls.name.replace(/ /g,"_")}.html` ), html );
}

function createModulePage( root, tpl, mainNav, mod, forcedName, forcedFile ) {

    let { varList, varData } = processVariables( mod.variables );
    let { funcList, funcData } = processFunctions( mod.functions );
    let classList = "";

    let html = tpl
        .replace( "[[ MAIN NAVIGATION ]]", mainNav )
        .replace( "[[ MODULE TITLE ]]", forcedName || mod.name )
        .replace( /\[\[ MODULE NAME \]\]/g, forcedName || mod.name )
        .replace( "[[ MODULE DESCRIPTION ]]", generateLinks( mod.description || "" ) )
        .replace( "[[ MODULE VARIABLES ]]", varList )
        .replace( "[[ MODULE FUNCTIONS ]]", funcList )
        .replace( "[[ MODULE CLASSES ]]", classList )
        .replace( "[[ MODULE VARIABLES INFO ]]", varData )
        .replace( "[[ MODULE FUNCTIONS INFO ]]", funcData )
    ;

    fs.writeFileSync( forcedFile || path.join( root, `module_${mod.name.replace(/ /g,"_")}.html` ), html );

}

function processFunction( func, name ) {
    var name = name || func.name; // Allow bypassing when providing class name for constructors
    var parameters = "";
    var syntax = `${name}(`;

    if( func !== null ) {
        func.params.forEach( ( param, index ) => {
            if( index > 0 ) syntax += ", "; else syntax += " ";
            if( param.optional ) syntax += "[ ";
            syntax += param.name;
            if( param.optional ) {
                if( param.optional && typeof param.default !== "undefined" ) syntax += "="+param.default;
                syntax += " ]";
            }
            if( index == func.params.length-1 ) syntax += " )";

            parameters += generateParameter( param );
        });

        if( func.params.length == 0 ) {
            parameters = "<tr><td><span class='none'>None</span></td></tr>";
        }
    }

    if( !syntax.endsWith( ")" ) ) syntax += ")";

    return { syntax, parameters };
}

function generateParameter( param ) {
    var
        name = param.name,
        type = param.type || "any",
        optional = param.optional || false,
        def = param.default,
        desc = param.description || ""
    ;

    var defVal = "<span class='none'>undefined</span>";
    if( typeof def !== "undefined" ) switch( type.toLowerCase() ) {
        case "-not-defined-": propType = "<span class='none'>any</span>"; break;
        case "any": defVal = `<span class="none">any</span>`; break;
        case "string": defVal = `<span class="string">${def}</span>`; break;
        case "number": defVal = `<span class="value">${def}</span>`; break;
        case "bool":
        case "boolean": defVal = `<span class="boolean">${def}</span>`; break;
        case "object":
            if( def[0] == "{" )
                defVal = `<pre><code class="javascript">${def}</code></pre>`
            else
                defVal = generateLinks("[["+def+"]]").replace(/\[/g,"").replace(/\]/g,"");
        break;
        default: defVal = generateLinks("[["+def+"]]").replace(/\[/g,"").replace(/\]/g,"");
    }

    return `
        <tr>
            <td>${ name }</td>
            <td>${ type }</td>
            <td>${ optional ? '<span class="optional">✓</span>' : '<span class="mandatory">✗</span>' }</td>
            <td>${ defVal }</td>
            <td>${ desc }</td>
        </tr>
    `;
}

function processVariables( vars ) {
    let varList = "", varData = "";

    varList = `
        <ul class="column small-12 medium-4 no-bullet">
            <li class="menu-group">
                <span>Variables</span>
                <ul class="no-bullet">
                    `
    ;

    varData = `
        <div class="row">
            <div class="column small-12">
                <h3>Variables</h3>`
    ;


    vars.forEach( v => {
        varList += `\n<li><a href="#var-${v.name.replace(/ /g,"-")}">.${v.name}</a></li>`;

        let varType = "";
        switch( v.type.toLowerCase() ) {
            case "-not-defined-": varType = "<span class='none'>any</span>"; break;
            case "any": varType = `<span class="none">any</span>`; break;
            case "string": varType = `<span class="string">String</span>`; break;
            case "number": varType = `<span class="value">Number</span>`; break;
            case "bool":
            case "boolean": varType = `<span class="boolean">Boolean</span>`; break;
            case "object": varType = `<span class="value">Object</span>`; break;
            default: varType = generateLinks("[["+v.type+"]]").replace(/\[/g,"").replace(/\]/g,"");
        }

        varData += `
                <div class="anchor" id="var-${v.name.replace(/ /g,"-")}"></div>
                <div class="row info">
                    <div class="column small-12">
                        <h5>.${v.name}</h5>
                        <div class="description">
                            <p>${v.description}</p>
                            <p><b>Type:</b>${varType}</p>
                        </div>
                    </div>
                </div>`
        ;

    });

    varList += `
                </ul>
            </li>
        </ul>
    `;

    varData += `
            </div>
        </div>
    `;

    if( vars.length == 0 ) {
        varList = '';
        varData = '';
    }

    return { varList, varData };
}

function processProperties( props ) {
    let propList = "", propData = "";

    propList = `
        <ul class="column small-12 medium-4 no-bullet">
            <li class="menu-group">
                <span>Properties</span>
                <ul class="no-bullet">
                    `
    ;

    propData = `
        <div class="row">
            <div class="column small-12">
                <h3>Properties</h3>`
    ;


    props.forEach( prop => {
        propList += `\n<li><a href="#prop-${prop.name.replace(/ /g,"-")}">.${prop.name}</a></li>`;

        let propType = "";
        switch( prop.type.toLowerCase() ) {
            case "-not-defined-": propType = "<span class='none'>any</span>"; break;
            case "any": propType = `<span class="none">any</span>`; break;
            case "string": propType = `<span class="string">String</span>`; break;
            case "number": propType = `<span class="value">Number</span>`; break;
            case "bool":
            case "boolean": propType = `<span class="boolean">Boolean</span>`; break;
            case "object": propType = `<span class="value">Object</span>`; break;
            default: propType = generateLinks("[["+prop.type+"]]").replace(/\[/g,"").replace(/\]/g,"");
        }

        propData += `
                <div class="anchor" id="prop-${prop.name.replace(/ /g,"-")}"></div>
                <div class="row info">
                    <div class="column small-12">
                        <h5>.${prop.name}</h5>
                        <div class="description">
                            <p>${prop.description}</p>
                            <p><b>Type:</b>${propType}</p>
                        </div>
                    </div>
                </div>`
        ;

    });

    propList += `
                </ul>
            </li>
        </ul>
    `;

    propData += `
            </div>
        </div>
    `;

    if( props.length == 0 ) {
        propList = '';
        propData = '';
    }

    return { propList, propData };
}

function processMethods( methods ) {
    let propList = "", propData = "";

    propList = `
        <ul class="column small-12 medium-4 no-bullet">
            <li class="menu-group">
                <span>Methods</span>
                <ul class="no-bullet">
                    `
    ;

    propData = `
        <div class="row">
            <div class="column small-12">
                <h3>Methods</h3>`
    ;


    methods.forEach( method => {
        let { syntax, parameters } = processFunction( method );
        propList += `\n<li><a href="#prop-${method.name.replace(/ /g,"-")}">.${method.name.replace(/ /g,"-")}()</a></li>`;

        let propType = "<span class='none'>undefined</span>";
        if( method.return !== null ) {
            switch( method.return.type.toLowerCase() ) {
                case "-not-defined-": propType = "<span class='none'>undefined</span>"; break;
                case "any": propType = `<span class="none">any</span>`; break;
                case "string": propType = `<span class="string">String</span>`; break;
                case "number": propType = `<span class="value">Number</span>`; break;
                case "bool":
                case "boolean": propType = `<span class="boolean">Boolean</span>`; break;
                case "object": propType = `<span class="value">Object</span>`; break;
                default: propType = generateLinks("[["+method.return.type+"]]").replace(/\[/g,"").replace(/\]/g,"");
            }
        }

        propData += `
                <div class="anchor" id="prop-${method.name.replace(/ /g,"-")}"></div>
                <div class="row info">
                    <div class="column small-12">
                        <h5>.${syntax}</h5>
                        <div class="description">
                            <p>${method.description}</p>

                            <table>
                                <thead>
                                    <tr>
                                        <td>Parameter</td>
                                        <td>Type</td>
                                        <td>Optional</td>
                                        <td>Default Value</td>
                                        <td>Description</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    ${ parameters }
                                </tbody>
                            </table>

                            <p>
                                <b>Returns:</b>${propType}
                                ${
                                    method.return ?
                                        method.return.description !=="" ? "<br>"+method.return.description : ""
                                    :   ""
                                }
                            </p>
                        </div>
                    </div>
                </div>`
        ;

    });

    propList += `
                </ul>
            </li>
        </ul>
    `;

    propData += `
            </div>
        </div>
    `;

    if( methods.length == 0 ) {
        propList = '';
        propData = '';
    }

    return { methodList:propList, methodData:propData };
}

function processFunctions( funcs ) {
    let propList = "", propData = "";

    propList = `
        <ul class="column small-12 medium-4 no-bullet">
            <li class="menu-group">
                <span>Functions</span>
                <ul class="no-bullet">
                    `
    ;

    propData = `
        <div class="row">
            <div class="column small-12">
                <h3>Functions</h3>`
    ;


    funcs.forEach( func => {
        let { syntax, parameters } = processFunction( func );
        propList += `\n<li><a href="#prop-${func.name.replace(/ /g,"-")}">.${func.name.replace(/ /g,"-")}()</a></li>`;

        let propType = "<span class='none'>undefined</span>";
        if( func.return !== null ) {
            switch( func.return.type.toLowerCase() ) {
                case "-not-defined-": propType = "<span class='none'>undefined</span>"; break;
                case "any": propType = `<span class="none">any</span>`; break;
                case "string": propType = `<span class="string">String</span>`; break;
                case "number": propType = `<span class="value">Number</span>`; break;
                case "bool":
                case "boolean": propType = `<span class="boolean">Boolean</span>`; break;
                case "object": propType = `<span class="value">Object</span>`; break;
                default: propType = generateLinks("[["+func.type+"]]").replace(/\[/g,"").replace(/\]/g,"");
            }
        }

        propData += `
                <div class="anchor" id="prop-${func.name.replace(/ /g,"-")}"></div>
                <div class="row info">
                    <div class="column small-12">
                        <h5>.${syntax}</h5>
                        <div class="description">
                            <p>${func.description}</p>

                            <table>
                                <thead>
                                    <tr>
                                        <td>Parameter</td>
                                        <td>Type</td>
                                        <td>Optional</td>
                                        <td>Default Value</td>
                                        <td>Description</td>
                                    </tr>
                                </thead>
                                <tbody>
                                    ${ parameters }
                                </tbody>
                            </table>

                            <p>
                                <b>Returns:</b>${propType}
                                ${
                                    func.return !== null ?
                                        func.return.description !== "" ? "<br>"+func.return.description : ""
                                    :   ""
                                }
                            </p>
                        </div>
                    </div>
                </div>`
        ;

    });

    propList += `
                </ul>
            </li>
        </ul>
    `;

    propData += `
            </div>
        </div>
    `;

    if( funcs.length == 0 ) {
        propList = '';
        propData = '';
    }

    return { funcList:propList, funcData:propData };
}
