(function($){
    "use strict";

    //
    // Sidebar Search logic
    //

    var $search = $( "#search" );
    var $terms = $("[data-term]");
    $search.on( "input", filterNav );

    function filterNav() {
        $terms.each( function( index, el ) {
            var $el = $(el);
            var term = $search.val().toLowerCase();
            if( term == "" ) $el.removeClass( "hide" );
            else if( $el.attr("data-term").indexOf( term ) > -1 ) $el.removeClass( "hide" );
            else $el.addClass( "hide" );
        });
    }

    //
    // Table of Content - Add animation when clicking on a member
    //

    var tocItem = $(".table-of-content a");
    tocItem.each( function( index, item ) {
        var $item = $( item );
        $item.on( "click", function() {
            var $target = $( $item.attr( "href" ) + " + div" );
            $target.removeClass( "show" );
            // Gotta wait 1millisecond at least, otherwise animation won't play
            setTimeout( function(){ $target.addClass( "show" ); }, 1 );
        });
    });

    //
    // Window show/hide scroller
    //

    $( window ).scroll( function() {
        var toc = $(".table-of-content");
        if( toc.length > 0 ) {
            if( ( toc.position().top + toc.height() ) - $( window ).scrollTop() <= 0 )
                $( "#scroll-up" ).addClass( "visible" );
            else
                $( "#scroll-up" ).removeClass( "visible" );
        }
        else {
            if( $(window).scrollTop() > $(window).height() )
                $( "#scroll-up" ).addClass( "visible" );
            else
                $( "#scroll-up" ).removeClass( "visible" );
        }
    });

    //
    // Scroller logic
    //

    $( "#scroll-up" ).on( "click", function() {
        var toc = $(".table-of-content");
        var pos = toc.length > 0 ?
            toc.position().top + toc.height()*0.5 - $(window).height()*0.5
            : 0
        ;
        $({ scroll: $(window).scrollTop() }).animate({ scroll: pos }, {
            duration: 500,
            easing: "swing",
            step: function( val ) {
                $( "html, body" ).scrollTop( val );
            }
        });
    });

    $( document ).foundation();
}(jQuery));
