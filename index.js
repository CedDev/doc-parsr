const fs = require( 'fs' );
const path = require( 'path' );
const find = require( 'fs-finder' );
const colors = require( 'colors' );
const marked = require( 'marked' );

let doc = {
    api: {
        modules: [],
        classes: [],
        functions: [],
        constants: [],
        variables: []
    },
    manual: [],
    source: []
};

module.exports = {
    run: function( config ) {

        "use strict";

        // Validate source
        if( !fs.existsSync( path.resolve( config.src ) ) ) {
            console.log( "[ ERROR ]: ".red + "The source path '"+config.src+"' is invalid!" );
            exit( 1 );
        }

        console.log( "" );

        let fileRegex = "";
        config.include.map( (regex,index) => {
            fileRegex += ( index==0?'':'|' ) + '(' + regex + ')';
        });

        let files = find.from( path.resolve( config.src ) ).findFiles( "<"+fileRegex+">");

        files.map( file => {
            var skip = false;
            config.exclude.map( ex => { if( file.indexOf( ex ) > -1 ) skip = true; } );

            if( !skip ) {

                console.log( "Parsing " + path.relative( path.resolve( "." ), file ) );

                if( /\.(jsx?|php)$/.test( file ) ) {
                    parseJS( fs.readFileSync( file, "utf8" ) );
                    // console.log( JSON.stringify( doc.api, null, 2 ) );
                }

                else if( /\.md$/.test( file )) {
                    parseMD( fs.readFileSync( file, "utf8" ), file );
                    // console.log( data );
                }

                else {
                    console.log( "[ Warning ]:".yellow, "Unkown file type '"+path.ext( file )+"'" );
                }

            }

        });

        if( !fs.existsSync( path.resolve( config.output ) ) ) {
            console.log( "\n[ Warning ]:".yellow, "The output folder will be created..." );
            fs.mkdirSync( path.resolve( config.output ) );
        }

        generateDoc( config.theme, config.output );

        exit( 0 );
    }
};

function parseJS( text ) {
    let regex = /\/\*\*([\S\s]*?)\*\//g;
    let m, res = {};

    let commands = [
        'desc', 'description',                 // Used for describing any statement
        'param', 'parameter',                  // Used to describe either a @function, a @method of a @constuctor
        'arg', 'argument',                     // alias for @param
        'prop', 'property', 'member',          // Used to describe members of a @class
        'const', 'constant',                   // Used to describe a constant
        'func', 'function', 'method',          // Used to describe a function, which might be a member of a class (@member)
        'ret', 'return', 'returns',            // Used to describe the return value of a @func or @method.
        'var', 'variable',                     // Used to define a variable that is not inside a @class
        'class', 'module',                     // Used to define the scope of the folloing statements
        'extends', 'extend',                   // Used to define the class that a class is extending
        'constructor', 'new',                  // Used to define the constructor @method of a @class
    ].sort( (a,b) => b.length-a.length );      // Make sure to place longuer words first, so that smaller words don't over-shadow them

    let statements = [];

    while(( m = regex.exec( text ) ) !== null ) {
        if( m.index === regex.lastIndex ) regex.lastIndex++;
        m.forEach(( match, index ) => {
            if( index < 1 ) return;

            // console.log( "{" );

            let description = "";
            let ss = [];
            match.trim().split('\n').forEach( line => {
                line = line.trim().replace( /^ ?\* ?/, "" );
                // console.log( "Parsing: "+line );
                if( ss.length == 0 && !line.startsWith('@') ) {
                    description += ( description == "" ? line : '\n' + line );
                }
                else if( !line.startsWith( '@' ) ) ss[ ss.length-1 ].description += '\n'+line
                else if( line.startsWith( '@' ) ) {
                    let i;
                    for( i=0; i<commands.length; i++ ) {
                        if( line.startsWith( '@'+commands[i] ) ) {
                            // Normalize commands to only one per type
                            var cmd;
                            switch( commands[i] ) {
                                case 'desc': case 'description': cmd = 'desc'; break;
                                case 'extends': case 'extend': cmd = 'extends'; break;
                                case 'arg': case 'argument': case 'param': case 'parameter': cmd = 'param'; break;
                                case 'prop': case 'property': case 'member': cmd = 'prop'; break;
                                case 'const': case 'constant': cmd = 'const'; break;
                                case 'func': case 'function': cmd = 'func'; break;
                                case 'ret': case 'return': case 'returns': cmd = 'return'; break;
                                case 'var': case 'variable': cmd = 'var'; break;
                                case 'constructor': case 'new': cmd = 'new'; break;
                                default: cmd = commands[i]
                            }

                            var p=0, c='', w='', s = {
                                command: cmd,
                                name: "",
                                description: "",
                                type: "-not-defined-",
                                optional: false
                            };

                            for( p=commands[i].length+2; p<line.length; p++ ) {
                                c = line[p];

                                // Try to read the type
                                if(
                                    commands[i] !== "desc" &&
                                    commands[i] !== "new" &&
                                    commands[i] !== "extends" &&
                                    c == '{' &&
                                    s.type === "-not-defined-"
                                ) {
                                    let pp = p;
                                    do {
                                        pp++;
                                        c = line[pp];
                                    }
                                    while( c != '}' && pp < line.length );

                                    if( pp < line.length ) {
                                        s.type = line.substring( p+1, pp ).trim();
                                        p = pp+1;
                                        continue;
                                    }
                                }

                                if(
                                    commands[i] !== "return" &&
                                    commands[i] !== "desc" &&
                                    commands[i] !== "new" &&
                                    commands[i] !== "extends" &&
                                    c == '[' &&
                                    s.name === ""
                                ) {
                                    let pp = p;
                                    do {
                                        pp++;
                                        c = line[pp];
                                    }
                                    while( c != ']' && pp < line.length );

                                    if( pp < line.length ) {
                                        var split = line.substring( p+1, pp ).trim().split("=");
                                        s.name = split[0].trim();
                                        if( split.length > 1 ) {
                                            split.shift();
                                            s.default = split.join("=").trim();
                                        }
                                        s.optional = true;
                                        p = pp+1;
                                        continue;
                                    }
                                }

                                if( ( c == ' ' || c == '\t' ) && w != "" ) {
                                    // if( w[0] == '{' && s.type === "-not-defined-" ) {
                                    //     s.type = w.substring( 1, w.length-1 );
                                    // }
                                    // else if( w[0] == '[' && s.name === "" ) {
                                    //     var split = w.substring( 1, w.length-1 ).split("=");
                                    //     s.name = split[0];
                                    //     if( split.length > 1 ) {
                                    //         split.shift();
                                    //         s.default = split.join("=");
                                    //     }
                                    //     s.optional = true;
                                    // }
                                    if(
                                        commands[i] !== "return" &&
                                        commands[i] !== "desc" &&
                                        commands[i] !== "new" &&
                                        s.name === ""
                                    ) {
                                        var split = w.split("=");
                                        s.name = split[0];
                                        if( split.length > 1 ) {
                                            split.shift();
                                            s.default = split.join("=");
                                        }
                                    }
                                    else {
                                        p -= w.length;
                                        w = "";
                                        break;
                                    }
                                    w = "";
                                }
                                else if( c != ' ' ) w += c;
                            }

                            // If w is not empty, we gotta treat it
                            // [ duplicate of above code ]
                            if(
                                commands[i] !== "return" &&
                                commands[i] !== "desc" &&
                                commands[i] !== "new" &&
                                s.name === ""
                            ) {
                                s.name = w;
                            }
                            else s.description += w;

                            s.description += line.substring( p );

                            // If the command's description is empty and 'description' isn't, add it to the command
                            if( s.description === "" && description !== "" ) {
                                s.description = description;
                                description = "";
                            }
                            // if( s.description !== "" ) console.log( "  Statement Description: '"+s.description+"'" );

                            ss.push( s );

                            break;
                        }
                    }
                }
            });

            statements = statements.concat( ss );
            // console.log( "}" );

            // console.log( "Description: " + description );
            // console.log( "Statements:" + JSON.stringify( statements, null, 2 ) );

        });
    }

    let i=0, target=doc.api, currentModule = null, currentClass = null, lastDef = null;
    for( i=0; i<statements.length; i++ ) switch( statements[i].command ) {
        case "module":
            doc.api.currentModule = statements[i].type;
            doc.api.modules.push({
                name: statements[i].name,
                constants: [],
                variables: [],
                functions: [],
                classes: []
            });
            currentModule = doc.api.modules[ doc.api.modules.length-1 ];
            target = currentModule;
        break;

        case "class":
            var cls = {
                name: statements[i].name,
                description: statements[i].description,
                new: null,
                props: [],
                methods: []
            };
            if( currentModule !== null ) currentModule.classes.push( cls );
            doc.api.classes.push( cls );
            currentClass = doc.api.classes[ doc.api.classes.length-1 ];
        break;

        case "var":
            if( currentModule !== null ) currentModule.variables.push( statements[i] );
            else doc.api.variables.push( statements[i] );
        break;
        case "constant":
            if( currentModule !== null ) currentModule.constants.push( statements[i] );
            else doc.api.constants.push( statements[i] );
        break;

        case "func":
            if( currentModule !== null ) currentModule.functions.push( statements[i] );
            else doc.api.functions.push( statements[i] );
            lastDef =
                currentModule !== null ? currentModule.functions[ currentModule.functions.length-1 ]
                : doc.api.functions[ doc.api.functions.length -1 ]
            ;
            lastDef.params = [];
            lastDef.return = null;
        break;

        case "extends":
            if( currentClass !== null ) {
                currentClass.extends = { name: statements[i].name };
            }
            else console.log( "WARNING: Ignoring 'extends' since not inside a class!" );
        break;

        case "new":
            if( currentClass !== null ) {
                if( currentClass.new === null ) {
                    currentClass.new = statements[i];
                    lastDef = currentClass.new;
                    lastDef.params = [];
                    lastDef.return = null;
                    lastDef.extends = null;
                }
                else console.log( "WARNING: Ignoring constructor '"+statements[i].name+"' since already found constructor '"+currentClass.new.name+"'." );
            }
            else console.log( "WARNING: Ingoring constructor '"+statements[i].name+"' since not inside a class." );
        break;

        case "prop":
            if( currentClass !== null ) currentClass.props.push( statements[i] );
            else console.log( "WARNING: Ingoring prop '"+statements[i].name+"' since not inside a class." );
        break;

        case "method":
            if( currentClass !== null ) {
                currentClass.methods.push( statements[i] );
                lastDef = currentClass.methods[ currentClass.methods.length-1 ];
                lastDef.params = [];
                lastDef.return = null;
            }
            else console.log( "WARNING: Ingoring prop '"+statements[i].name+"' since not inside a class." );
        break;

        case "param":
            if( lastDef !== null ) lastDef.params.push( statements[i] );
            else console.log( "WARNING: Ignoring param '"+statements[i].name+"' since not inside a function/method." );
        break;

        case "return":
            if( lastDef !== null ) {
                if( lastDef.return === null ) lastDef.return = statements[i];
                else console.log( 'WARNING: Ignoring return since it was already defined.' );
            }
            else console.log( "WARNING: Ignoring param '"+statements[i].name+"' since not inside a function/method." );
        break;
    }
}

function parseMD( text, filename ) {
    let title = path.basename( filename, path.extname( filename ) );
    let lines = text.trim().split('\n');
    if( lines[0].trim().startsWith( "[//]: # (" ) ) {
        title = lines[0].trim().substring( 9, lines[0].trim().length-1 );
    }
    doc.manual.push({
        name: title.trim(),
        html: marked( text )
    });
}

function generateDoc( theme, docPath ) {

    // Try to load internal theme
    if( fs.existsSync( path.join( __dirname, "themes", theme ) ) ) {
        theme = path.join( __dirname, "themes", theme, `${theme}.js` );
    }

    if( !fs.existsSync( theme ) ) {
        console.log( "[ Error ]".red + ": The specified theme could not be located!" );
        exit(1);
    }

    let themeGenerator = require( theme );
    try {
        themeGenerator( doc, docPath );
        exit( 0 );
    }
    catch( err ) {
        console.log( "[ Error ]".red + ": The specified theme is not a valid theme." );
        console.log( err );
        exit( 1 );
    }

}

function exit( code ) {
    console.log( "\n" );
    process.exit( code );
}
