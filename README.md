[//]: # ( Home )

# Doc Parsr

---

doc-parsr is a documentation parser similar to JSDoc. I've built it since I needed a way to parse only comments ( no code ) and the ability to add-remove 'keywords' as I wanted.

For instance, I want to eventually add React and Redux support in my parser.

---

### What does the final product looks like?

Here you can see some screenshots of the included themes:

#### Simple -  The default theme
![Simple Theme Preview](https://image.prntscr.com/image/N4bn3dC9TOCU-xjJ-_VhcQ.png)

#### Darky -  A dark alternative
![Darky Theme Preview](http://i.imgur.com/65EQ1sj.png)

---
### Installation

1. Run the command `npm install doc-parsr`

That's it, you're ready to use doc-parsr!

---
### Configuration

doc-parsr will look for `.doc-parsr.json` in your project's root folder. If it can't find it, it will use the following default values:

```json
{
    "src": ".",
    "include": [ "\\.jsx?$", "\\.md$" ],
    "exclude": [ "node_modules/", "docs/", ".meteor/" ],
    "output": "./docs",
    "theme": "simple"
}
```

- **src**: The path to the files to parse;
- **include**: An array of regex to match filenames ( meant to be used for file extensions );
- **exclude**: Array of strings that should not be present in the *path* of the included files;
- **output**: The target folder where to generate the documentation pages;
- **theme**: The theme to use. Theme can be one of those in the `themes` folder of the package, or a path to a custom theme.

---
### Usage

To build your documentation, create a script entry in your `package.json` or manually run the following command: `doc-parsr`.

doc-parsr will only read your comments starting with `/**` ( similar to JSDoc ).

Here is a list of currently supported keywords:

| Keyword | Description | Alias |
|---------|--------------|-------|
| **module** | Anything in the same file, following this statement, will be considered as part of a module and will hence not be shown in the `Global` area, except for classes. | - |
| **class** | Defines a class | - |
| **@constructor** | Used inside a `@class`, defines the constructor. | `@new` |
| **extends** | Used with `@class`, defines which class the current one is extending. | - |
| **prop** | Used inside `@class`, defines a class property. | `@property` |
| **var** | Used inside `@module` or on it's own, defines a variable. | `@variable` |
| **method** | Used inside `@class`, defines a class method. | - |
| **func** | Used insde `@module` or on it's own, defines a function. | `@function` |
| **param** | Used inside both `@constructor`, `@class` and `@function`, defines a parameter. | `@arg`, `@argument`, `@parameter` |
| **type** | Used inside `@prop`, and `@var`, defines the type of the arg/var. | - |
| **returns** | Used inside `@method`, `@function`, defines the type of the returned value. | `@return` |
| **desc** | Add a description for the last keyword. | `@description` |

---
### Example

```js
/*
    Remember that this parser doesn't care about code, just about comments.
    And more specifically, those starting with double-stars.
    ( which means this comment will be ignored. )
*/

/**
 * Toggles some debug tools.
 * Note that this will NOT be inside the module [[MyModule]]
 * @var {Boolean} DEBUG
 */

/** @module MyModule */

/**
 * Description can reference other classes like [[MyOtherClass]]
 * @class MyClass
 */

/** @prop {String} name A prop for the name */

/**
 * This method does some stuff
 * @method aMethod
 * @param {Number} count The amount of recursivness
 * @param {Object} [ config={ a:2, b:"Test" } ] Some random configs
 * @param {String} [ mode="Some Mode" ] The mode to use
 * @returns {Boolean} Weither or not it succeeded
 */
```

---
### Changelog

##### [Unreleased]
- No changes logged

##### [ 1.0.1 ] - 2017/08/08
###### Note
- Completed and released

##### [ 1.0.4 ] - 2017/08/08
###### Added
- README.md

##### [ 1.0.6 ] - 2017/08/08
###### Added
- Support for `extends`

##### [ 1.0.8 ] - 2017/08/08
###### Changed
- Updated REAMDME.md

##### [ 1.0.9 ] - 2017/08/09
###### Updated Theme 'simple'
- Added search in sidebar

##### [ 1.0.15 ] - 2017/08/11
###### Added
- Support for PHP

##### [ 1.1.0 ] - 2017/08/15
###### Added
- Theme 'darky', WIP
