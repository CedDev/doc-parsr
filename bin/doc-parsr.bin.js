#! /usr/bin/env node

const fs = require( 'fs' );
const path = require( 'path' );
const clear = require( 'clear' );
const parsr = require( '../index' );

clear();

let configPath = "", config = {};

if( process.argv.length > 2 ) configPath = path.resolve( process.argv[2] );
else configPath = path.resolve( "./.doc-parsr.json" );

if( fs.existsSync( configPath ) ) {
    console.log( "Using", path.basename( configPath ), "..." );
    config = require( configPath );
}
else console.log( "Couldn't locate config, using defaults..." );

// Set default if not provided
config.src = config.src || path.resolve( "." );
config.include = config.include || [ "\\.jsx?$", "\\.md$" ];
config.exclude = config.exclude || [ "node_modules/", ".meteor/", "docs/" ];
config.output = config.output || path.resolve( "./docs" );
config.theme = config.theme || "simple";

process.exit( parsr.run( config ) );
